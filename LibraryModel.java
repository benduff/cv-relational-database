/*
 * LibraryModel.java
 * Author: Benjamin Duff, DUFFBENJ, 300479838
 * Created on:	13/10/2019
 */



import javax.swing.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;
import java.sql.SQLException;

public class LibraryModel {
	
	private String conUrl;
	private String conUserid;
	private String conPassword;
	
	private Connection con = null;
	private Statement s = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Statement s_custCheck = null;
	private ResultSet rs_custCheck = null;
	private Statement s_bookCheck = null;
	private ResultSet rs_bookCheck = null;
	private PreparedStatement ps_loanUpdate = null;
	private PreparedStatement ps_bookUpdate = null;
	private Statement s_lockBook = null;
	private Statement s_checkLoan = null;
	private ResultSet rs_checkLoan = null;
	private PreparedStatement ps_authorUpdate = null;

	private String query;
	

    // For use in creating dialogs and making them modal
    //private JFrame dialogParent;

    public LibraryModel(JFrame parent, String userid, String password) {
    	//dialogParent = parent;
    	
    	conUrl = "jdbc:postgresql://db.ecs.vuw.ac.nz/" + userid + "_jdbc";
    	conUserid = userid;
    	conPassword = password;
    	
    	// register postgresql driver
    	try {
    		Class.forName("org.postgresql.Driver");
    	} catch (ClassNotFoundException cnfe) {
    		System.out.println("Cannot find driver class");
    	}
    	
    	// establish a connection
    	try {
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    	} catch (SQLException sqlex) {
    		System.out.println("Cannot connect");
    		System.out.println(sqlex.getMessage());
    	}
    }
 
    public String bookLookup(int isbn) {
    	// string builder to store/return query output
		StringBuilder toShow = new StringBuilder();
		
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection ready only for query
    		con.setReadOnly(true);
    		
    		// enable auto commit for single statement query
    		con.setAutoCommit(true);
    		
    		// query:
    		query = "SELECT b.isbn, b.title, b.edition_no, "
    				+ "STRING_AGG(CONCAT(RTRIM(a.name), ' ', RTRIM(a.surname), ' (', CAST(a.authorid AS text), ')'), ',  ' ORDER BY ba.authorseqno), "
    				+ "b.numleft, b.numofcop "
    				+ "FROM Author a NATURAL JOIN Book_Author ba FULL OUTER JOIN Book b "
    				+ "ON ba.isbn=b.isbn "
    				+ "WHERE b.ISBN = ? "
    				+ "GROUP BY b.ISBN";
    		
    		
    		// create statements and execute query
    		s = con.createStatement();
    		ps = con.prepareStatement(query);
    		ps.setInt(1, isbn);
    		rs = ps.executeQuery();
    		
    		// get query result and add to string builder all relevant information in easy to view format
    		while(rs.next()) {
    			toShow.append("\nISBN:  "+rs.getString(1));
    			toShow.append("\nTitle:  "+rs.getString(2));
    			toShow.append("\nEdition:  "+rs.getString(3));
    			toShow.append("\nAuthor(s):  "+rs.getString(4));
    			toShow.append("\nAvailable:  "+rs.getString(5)+" / "+rs.getString(6));
    			toShow.append("\n");
        	}
    		
    		// if string builder is empty there must be no query result
    		if (toShow.length() == 0) {
    			toShow.append("no books with isbn: "+isbn);
    		}
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error when doing booklookup");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    	
    	return toShow.toString();
    }

    public String showCatalogue() {
    	// string builder to store/return query output
    	StringBuilder toShow = new StringBuilder();
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection ready only for query
    		con.setReadOnly(true);
    		
    		// enable auto commit for single statement query
    		con.setAutoCommit(true);
    		
    		// query:
    		query = "SELECT isbn, title FROM book WHERE isbn>0 ORDER BY isbn";
    		
    		// create statements and execute query
    		s = con.createStatement();
    		rs = s.executeQuery(query);
    		
    		// add query output to string builder
    		// NOTE my choice for catalogue is only isbn and title tuples ordered in rows for readability and space efficiency
    		// if user wants more info on a book they can use book lookup
    		while(rs.next()) {
    			toShow.append(rs.getString(1)+"       "+rs.getString(2)+"\n");
        	}
    		
    		// if string builder is empty there must be no query result
    		if (toShow.length() == 0) {
    			toShow.append("no books in catalogue");
    		}
    		// otherwise add column headers 
    		else {
    			toShow.insert(0, "ISBN:    Title:\n");
    		}
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error when doing showcatalogue");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    	
    	return toShow.toString();
    }

    public String showLoanedBooks() {
    	// string builder to store/return query output
    	StringBuilder toShow = new StringBuilder();
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection ready only for query
    		con.setReadOnly(true);
    		 		
    		// enable auto commit for single statement query
    		con.setAutoCommit(true);
    		
    		// query:
    		query = "SELECT customerid, isbn, duedate FROM cust_book ORDER BY customerid, duedate";
    		
    		// create statements and execute query
    		s = con.createStatement();
    		rs = s.executeQuery(query);
    		
    		// add query output to string builder
    		// spaces for formatting
    		while(rs.next()) {
    			toShow.append(rs.getString(1)+"              "+rs.getString(2)+"         "+rs.getString(3)+"\n");
        	}
    		
    		// if string builder is empty there must be no query result
    		if (toShow.length() == 0) {
    			toShow.append("no books on loan");
    		}
    		// otherwise add column headers 
    		else {
    			toShow.insert(0, "CID:        ISBN:       Due Date:   \n");
    		}
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error when doing showloanedbooks");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    	
    	return toShow.toString();
    }
    
    public String showAuthor(int authorID) {
    	// string builder to store/return query output
		StringBuilder toShow = new StringBuilder();
		
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection ready only for query
    		con.setReadOnly(true);
    		
    		// enable auto commit for single statement query
    		con.setAutoCommit(true);
    		
    		// query:
    		query = "SELECT a.authorid, RTRIM(a.name), a.surname, STRING_AGG(CONCAT('(', CAST(b.isbn AS text), ') ', RTRIM(b.title)), ',  ') "
    				+ "FROM Author a NATURAL JOIN Book_Author ba NATURAL JOIN Book b "
    				+ "WHERE a.authorid = ? "
    				+ "GROUP BY a.authorid";

    		// create statements and execute query
    		s = con.createStatement();
    		ps = con.prepareStatement(query);
    		ps.setInt(1, authorID);
    		rs = ps.executeQuery();
    		
    		// get query result and add to string builder all relevant information in easy to view format
    		while(rs.next()) {
    			toShow.append("\nAuthorID:  "+rs.getString(1));
    			toShow.append("\nName:  "+rs.getString(2)+" "+rs.getString(3));
    			toShow.append("\nBooks:  "+rs.getString(4));
    			toShow.append("\n");
        	}
    		
    		// if string builder is empty there must be no query result
    		if (toShow.length() == 0) {
    			toShow.append("no authors with authorID: "+authorID);
    		}
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error when doing authorlookup");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    	
    	return toShow.toString();    	
    }

    public String showAllAuthors() {
    	// string builder to store/return query output
    	StringBuilder toShow = new StringBuilder();
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection ready only for query
    		con.setReadOnly(true);
    		
    		// enable auto commit for single statement query
    		con.setAutoCommit(true);
    		
    		// query:
    		query = "SELECT a.authorid, RTRIM(a.name), RTRIM(a.surname), ARRAY_AGG(ba.isbn ORDER BY ba.isbn) "
    				+ "FROM Author a NATURAL JOIN Book_Author ba "
    				+ "GROUP BY a.authorid";
    		
    		// create statements and execute query
    		s = con.createStatement();
    		rs = s.executeQuery(query);
    		
    		// add query output to string builder
    		while(rs.next()) {
    			// determine required spacing between author names and books for alignment (always positive because max length isbn+name+surname=34)
    			int spaces = 35 - (rs.getString(1)+rs.getString(2)+rs.getString(3)).length();
    			toShow.append(rs.getString(1)+"          "+rs.getString(2)+" "+rs.getString(3)+" ".repeat(spaces)+rs.getString(4)+"\n");
    		}
    		
    		// if string builder is empty there must be no query result
    		if (toShow.length() == 0) {
    			toShow.append("no authors in author table");
    		}
    		// otherwise add column headers 
    		// NOTE: there are issues with spacing but couldn't get it to work without fixed width font
    		else {
    			toShow.insert(0, "AID:    Name:                               Books:\n");
    		}
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error when doing showallauthors");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    	
    	return toShow.toString();
    }

    public String showCustomer(int customerID) {
    	// string builder to store/return query output
		StringBuilder toShow = new StringBuilder();
		
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection ready only for query
    		con.setReadOnly(true);
    		
    		// enable auto commit for single statement query
    		con.setAutoCommit(true);
    		
    		// query:
    		query = "SELECT c.customerid, RTRIM(c.f_name), c.l_name, c.city, "
    				+ "STRING_AGG(CONCAT('ISBN: ', CAST(cb.isbn AS text), ',  Due: ', cb.duedate), '\n                            ') "
    				+ "FROM customer c FULL OUTER JOIN cust_book cb "
    				+ "ON c.customerid = cb.customerid "
    				+ "WHERE c.customerid = ? "
    				+ "GROUP BY c.customerid";

    		// create statements and execute query
    		s = con.createStatement();
    		ps = con.prepareStatement(query);
    		ps.setInt(1, customerID);
    		rs = ps.executeQuery();
    		
    		// get query result and add to string builder all relevant information in easy to view format
    		while(rs.next()) {
    			toShow.append("\nCustomerID:   "+rs.getString(1));
    			toShow.append("\nName:   "+rs.getString(2)+" "+rs.getString(3));
    			toShow.append("\nCity:   "+rs.getString(4));
    			toShow.append("\nLoaned Books:   "+rs.getString(5));
    			toShow.append("\n");
        	}
    		
    		// if string builder is empty there must be no query result
    		if (toShow.length() == 0) {
    			toShow.append("no customer with customerID: "+customerID);
    		}
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error when doing authorlookup");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    	
    	return toShow.toString();  
    }

    public String showAllCustomers() {
    	// string builder to store/return query output
    	StringBuilder toShow = new StringBuilder();
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection ready only for query
    		con.setReadOnly(true);
    		
    		// enable auto commit for single statement query
    		con.setAutoCommit(true);
    		
    		// query:
    		query = "SELECT customerid, RTRIM(f_name), RTRIM(l_name), city "
    				+ "FROM Customer "
    				+ "WHERE customerid>0 "
    				+ "ORDER BY customerid";
    		
    		// create statements and execute query
    		s = con.createStatement();
    		rs = s.executeQuery(query);
    		
    		// add query output to string builder
    		while(rs.next()) {
    			// determine required spacing between names and city for alignment (always positive because max length customerid+f_name+l_name=34)
    			int spaces = 40 - (rs.getString(1)+rs.getString(2)+rs.getString(3)).length();
    			toShow.append(rs.getString(1)+"    "+rs.getString(2)+" "+rs.getString(3)+" ".repeat(spaces)+rs.getString(4)+"\n");
        	}
    		
    		// if string builder is empty there must be no query result
    		if (toShow.length() == 0) {
    			toShow.append("no customers in customer table");
    		}
    		
    		// otherwise add column headers 
    		// NOTE: there are issues with spacing but couldn't get it to work without fixed width font
    		else {
    			toShow.insert(0, "CID:    Name:                               City:\n");
    		}
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error when doing showallcustomers");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    	
    	return toShow.toString();
    }

    public String borrowBook(int isbn, int customerID,
			     int day, int month, int year) {
    	
    	// convert date to sql.Date type
    	LocalDate dueDate = LocalDate.of(year, month, day);
		long dueDays = dueDate.toEpochDay();
		long dueMillis = TimeUnit.DAYS.toMillis(dueDays);
		java.sql.Date SQLdueDate = new java.sql.Date(dueMillis);
		
		// string to return either error message or success message
		String returnString;
		
		String SQLCheckCustomer = 
				"SELECT * "
				+ "FROM Customer "
				+ "WHERE CustomerId="+customerID
				+ " FOR UPDATE";
		
		String SQLCheckBook =
				"SELECT * "
				+ "FROM Book "
				+ "WHERE ISBN="+isbn
				+ " AND NumLeft>0 "
				+ " FOR UPDATE";
						
    	String SQLInsertCust_Book = 
    			"INSERT INTO cust_book "
    			+ "VALUES (?,?,?)";

    	String SQLUpdateBook = 
    			"UPDATE book "
    			+ "SET NumLeft = NumLeft-1 "
    			+ "WHERE ISBN ="+isbn;
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection read only to false for update and insert
    		con.setReadOnly(false);
    		
    		// disable auto commit for multi statement transaction
    		con.setAutoCommit(false);
    		
    		s_custCheck = con.createStatement();
    		rs_custCheck = s_custCheck.executeQuery(SQLCheckCustomer);
    			
    		// do not proceed if no matching customer
    		if (!rs_custCheck.next()) {
    			returnString = "error: no customer with customerid: "+customerID;
    			System.out.println("rolling back transaction");
    			con.rollback();
    			return returnString;
    		}
    		
    		s_bookCheck = con.createStatement();
    		rs_bookCheck = s_bookCheck.executeQuery(SQLCheckBook);
    		
    		// do not proceed if no matching book or no copies available
    		if (!rs_bookCheck.next()) {
    			returnString = "error: no book with isbn: "+isbn+", OR no copies of book available";
    			System.out.println("rolling back transaction");
    			con.rollback();
    			return returnString;
    		}

    		// add loaned book to cust_book table
    		ps_loanUpdate = con.prepareStatement(SQLInsertCust_Book);
    		ps_loanUpdate.setInt(1, isbn);
    		ps_loanUpdate.setDate(2, SQLdueDate);
    		ps_loanUpdate.setInt(3, customerID);
    		ps_loanUpdate.executeUpdate();
    		
    		// dialog box pop up to test transaction locks
    		JOptionPane.showConfirmDialog(null, "click to continue", null, 1);
    		
    		// update numleft in book table
    		ps_bookUpdate = con.prepareStatement(SQLUpdateBook);		
    		ps_bookUpdate.executeUpdate();
    		
    		con.commit();
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error loaning book");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    	
    	returnString = "New loan added to customer "+customerID+" for book "+isbn+", due back "+dueDate.toString();	
    	return returnString;
    }

    public String returnBook(int isbn, int customerid) {
    	// string to return either error or success message
    	String returnString;
    	
    	String SQLLockBook =
    			"SELECT * "
    			+ "FROM Book "
    			+ "WHERE isbn="+isbn
    			+ " FOR UPDATE";
    	
    	String SQLCheckCust_Book =
    			"SELECT * "
    			+ "FROM Cust_Book "
    			+ "WHERE isbn="+isbn
    			+ " AND customerid="+customerid
    			+ " FOR UPDATE";
    	
    	String SQLDeleteCust_Book =
    			"DELETE FROM Cust_Book "
    			+ "WHERE isbn="+isbn
    			+ " AND customerid="+customerid;
    	
    	String SQLUpdateBook = 
    			"UPDATE book "
    			+ "SET NumLeft = NumLeft+1 "
    			+ "WHERE ISBN ="+isbn;
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection read only to false for update and insert
    		con.setReadOnly(false);
    		
    		// disable auto commit for multi statement transaction
    		con.setAutoCommit(false);
    		
    		// lock book for numleft update
    		s_lockBook = con.createStatement();
    		s_lockBook.executeQuery(SQLLockBook);
    		
    		// check book loan exists
    		s_checkLoan = con.createStatement();
    		rs_checkLoan = s_checkLoan.executeQuery(SQLCheckCust_Book);
    		
    		// do not proceed if no loan exists
    		if (!rs_checkLoan.next()) {
    			returnString = "error: no loan of "+isbn+" for customer "+customerid;
    			System.out.println("rolling back transaction");
    			con.rollback();
    			return returnString;
    		}
    		
    		// delete loaned book from cust_book table
    		ps_loanUpdate = con.prepareStatement(SQLDeleteCust_Book);
    		ps_loanUpdate.executeUpdate();
    		
    		// update numleft in book table
    		ps_bookUpdate = con.prepareStatement(SQLUpdateBook);
    		ps_bookUpdate.executeUpdate();
    		
    		con.commit();
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error returning book");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    
    	returnString = "Customer "+customerid+" successfully returned book "+isbn;
    	return returnString;
    }
    
    public void closeDBConnection(){
    	try {
    		if (con!=null)	{ con.close();}
    		if (s!=null)	{ s.close();}
    		if (ps!=null)	{ ps.close();}
    		if (rs!=null)	{rs.close();}
    		if (s_custCheck!=null)	{s_custCheck.close();}
    		if (rs_custCheck!=null)	{rs_custCheck.close();}
    		if (s_bookCheck!=null)	{s_bookCheck.close();}
    		if (rs_bookCheck!=null)	{rs_bookCheck.close();}
    		if (ps_loanUpdate!=null){ps_loanUpdate.close();}
    		if (ps_bookUpdate!=null){ps_bookUpdate.close();}
    		if (s_lockBook!=null)	{s_lockBook.close();}
    		if (s_checkLoan!=null)	{s_checkLoan.close();}
    		if (rs_checkLoan!=null)	{rs_checkLoan.close();}
    		if (ps_authorUpdate!=null)	{ps_authorUpdate.close();}
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
    	}
    }

    public String deleteCus(int customerID) {
    	// string to return either error or success message
    	String returnString;
    	
    	String SQLCheckCustomer = 
    			"SELECT * "
    			+ "FROM Customer "
    			+ "WHERE customerid="+customerID
    			+ " FOR UPDATE";
    	
    	String SQLCheckLoans = 
    			"SELECT * "
    			+ "FROM Cust_Book "
    			+ "WHERE customerid="+customerID
    			+ " FOR UPDATE";
    	
    	String SQLDeleteCustomer =
    			"DELETE FROM Customer "
    			+ "WHERE customerid="+customerID;
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection read only to false for update and insert
    		con.setReadOnly(false);
    		
    		// disable auto commit for multi statement transaction
    		con.setAutoCommit(false);
    		
    		s_custCheck = con.createStatement();
    		rs_custCheck = s_custCheck.executeQuery(SQLCheckCustomer);
    			
    		// do not proceed if no matching customer
    		if (!rs_custCheck.next()) {
    			returnString = "error: no customer with customerid: "+customerID;
    			System.out.println("rolling back transaction");
    			con.rollback();
    			return returnString;
    		}
    		
    		s_checkLoan = con.createStatement();
    		rs_checkLoan = s_checkLoan.executeQuery(SQLCheckLoans);
    		
    		// do not proceed if customer currently has books on loan (violates foreign key)
    		if (rs_checkLoan.next()) {
    			returnString = "error: customer currently has books on loan and cannot be deleted";
    			System.out.println("rolling back transaction");
    			con.rollback();
    			return returnString;
    		}
    		
    		// delete this customer from customer table
    		ps = con.prepareStatement(SQLDeleteCustomer);
    		ps.executeUpdate();
	
    		con.commit();
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error deleting customer");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    
    	returnString = "Customer "+customerID+" successfully deleted";
    	return returnString;
    }

    public String deleteAuthor(int authorID) {
    	// string to return either error or success message
    	String returnString;
    	
    	String SQLDeleteAuthor =
    			"DELETE FROM Author "
    			+ "WHERE authorid="+authorID;
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection read only to false for update and insert
    		con.setReadOnly(false);
    		
    		// disable auto commit for multi statement transaction
    		con.setAutoCommit(false);
    		
    		// delete this author from author table
    		ps_authorUpdate = con.prepareStatement(SQLDeleteAuthor);
    		ps_authorUpdate.executeUpdate();
	
    		con.commit();
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error deleting author");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    
    	returnString = "Author "+authorID+" successfully deleted";
    	return returnString;
    }

    public String deleteBook(int isbn) {
    	// string to return either error or success message
    	String returnString;
    	
    	String SQLCheckBook = 
    			"SELECT * "
    			+ "FROM Book "
    			+ "WHERE isbn="+isbn
    			+ " FOR UPDATE";
    	
    	String SQLCheckLoans = 
    			"SELECT * "
    			+ "FROM Cust_Book "
    			+ "WHERE isbn="+isbn
    			+ " FOR UPDATE";
    	
    	String SQLDeleteBook =
    			"DELETE FROM Book "
    			+ "WHERE isbn="+isbn;
    	
    	try {
    		// establish connection
    		con = DriverManager.getConnection(conUrl, conUserid, conPassword);
    		
    		// set connection read only to false for update and insert
    		con.setReadOnly(false);
    		
    		// disable auto commit for multi statement transaction
    		con.setAutoCommit(false);
    		
    		s_bookCheck = con.createStatement();
    		rs_bookCheck = s_bookCheck.executeQuery(SQLCheckBook);
    			
    		// do not proceed if no matching book
    		if (!rs_bookCheck.next()) {
    			returnString = "error: no book with isbn: "+isbn;
    			System.out.println("rolling back transaction");
    			con.rollback();
    			return returnString;
    		}
    		
    		s_checkLoan = con.createStatement();
    		rs_checkLoan = s_checkLoan.executeQuery(SQLCheckLoans);
    		
    		// do not proceed if book is currently on loan (violates foreign key)
    		if (rs_checkLoan.next()) {
    			returnString = "error: book is currently on loan and cannot be deleted";
    			System.out.println("rolling back transaction");
    			con.rollback();
    			return returnString;
    		}
    		
    		// delete this customer from customer table
    		ps = con.prepareStatement(SQLDeleteBook);
    		ps.executeUpdate();
	
    		con.commit();
    		
    	} catch (SQLException sqlex) {
    		System.out.println("error deleting book");
    		System.out.println(sqlex.getMessage());
    	} finally {
    		closeDBConnection();
    	}
    
    	returnString = "Book "+isbn+" successfully deleted";
    	return returnString;
    }
    
}